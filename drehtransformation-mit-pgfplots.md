# Titel #

Représenter un vissage à l'aide de pgfplots

## Text ##

J'essaie de reproduire la figure ci-dessous à l'aide de `pgfplots` :

<deine Figur>

J'ai vu une explication sur
*[mathematica stackexchange](https://mathematica.stackexchange.com/questions/37698/how-to-plot-a-certain-surface-what-is-its-parametric-equation)*. Elle
va me servir d'exemple : je vais procéder de même. Ainsi vais-je tout d'abord
dessiner une coupe de la figure, puis visser celle-ci en suivant le périmètre
d'un cercle.

La vue en coupe est obtenue grâce au code suivant :

    \documentclass[border=10pt]{standalone}
    \usepackage{pgfplots}
    \usepgfplotslibrary{polar}
    \begin{document}
    \begin{tikzpicture}
      \begin{polaraxis}
        \addplot[mark=none, domain=0:360, samples=100]
          {sin(3*x) + 1.25}; 
      \end{polaraxis}
    \end{tikzpicture}
    \end{document}

Puis je donne de celle-ci une vue en trois dimensions.  Je colore ma figure pour
plus de lisibilité.  Par souci de simplification, je place la figure au
niveau 0.  Il est toujours possible de modifier cela ou de changer le point de
vue.

    \documentclass[border=10pt]{standalone}
    \usepackage{pgfplots}
    \begin{document}
    \begin{tikzpicture}
      \begin{axis}
        \addplot3 [domain=0:360, samples=60, fill=blue!30, opacity=0.8]
          ( {cos(x)*(sin(3*x) + 1.25)},
            {sin(x)*(sin(3*x) + 1.25)}, 0 );
      \end{axis}
    \end{tikzpicture}
    \end{document}

On aura remarqué que l'on est passé de coordonnées polaires
à des coordonnées cartésiennes, qui ont été obtenues entre-temps
par la fonction suivante :

<!-- I don't understand your comment with data cs=polar,
and also why this piece of code is not included in the final one -->

    \pgfplotsset{
      polar/.style = {
        x filter/.code = \pgfmathparse{cos(deg(rawx))*rawy},
        y filter/.code = \pgfmathparse{sin(deg(rawx))*rawy}
      }
    }

Il ne nous reste plus qu'à faire tourner la figure par rapport à son centre,
pendant que celui-ci décrit un cercle selon un axe perpendiculaire au plan dans
lequel s'inscrit la figure. On trouvera sur
*[TeX stackexchange](https://tex.stackexchange.com/questions/142152/how-to-produce-a-3d-surface-plot-by-rotating-the-2d-plot-of-a-function)*
un premier exemple de rotation sur l'axe Z, obtenu grâce à la fonction
`({sin(x)*y},{cos(x)*y},{ <function of y> })`.

Outre le fil consacré à *mathematica* cité plus haut, on peut s'inspirer de la
[documentation](http://reference.wolfram.com/language/ref/RotationTransform.html)
dudit [logiciel](https://fr.wikipedia.org/wiki/Mathematica).

Car la question demeure : comment réaliser cette figure avec `pgfplots` ?

PS : cette question est la traduction de
[celle-ci](http://texwelt.de/wissen/fragen/7436/drehtransformation-mit-pgfplots),
posée sur le forum germanophone [texwelt.de](http://texwelt.de/wissen).

## Antwort ##

Il s'agit de réaliser une sorte de tore ouvert vissé, en fait.  On peut
s'inspirer de [ce
fil](https://tex.stackexchange.com/questions/142152/how-to-produce-a-3d-surface-plot-by-rotating-the-2d-plot-of-a-function)
sur *TeX stackexchange*, inttulé *Comment dessiner un tore ?*

On y lira qu'un tore peut par exemple être obtenu par l'équation :

    x(t,s) = (2+cos(t))*cos(s+pi/2) 
    y(t,s) = (2+cos(t))*sin(s+pi/2) 
    z(t,s) = sin(t)

dont `z` et `t` appartiennent à l'intervalle `[0,2\pi]`.

<!-- Here should be posted the complete code to get a basic torus, i.e. a donut -->

Comment adapter ceci à notre figure ?

Il s'agit de dessiner en 3D la figure 2D obtenu par les équations suivantes :

    x(t) = sin(3t)cos(t)
    y(t) = sin(3t)sin(t)

Un peu de gamberge nous amène à :

    x(t,s) = (4+(sin(3*(t))+1.25)*cos(t))*cos(s)
    y(t,s) = (4+(sin(3*(t))+1.25)*cos(t))*sin(s)
    z(t,s) = ((sin(3*(t))+1.25)*sin(t))


Ce qui nous donne, pour une valeur définie de `s` et `t\in[0,2\pi]`,
le code et la figure suivants :

<!-- first figure from cmhugues on TeX SE
it should be followed by its complete code !-->

À partir de là, on peut faire varier `s`, par exemple si l'on a `s\in[0,\pi]`,
on obtient les code et figure suivants :

<!-- second figure from cmhugues on TeX SE (the half donut)
it should be followed by its complete code
it may sound redundant but it's useful ! -->

Fort bien, mais de vissage, point.

Il est obtenu par le code suivant :

    \documentclass[border=10pt]{standalone}
    \usepackage{pgfplots}
    \begin{document}
    \begin{tikzpicture}
      \begin{axis}[axis equal]
        \addplot3[
          surf,
          domain    = 0:360,
          y domain  = 0:360,
          samples   = 100,
          samples y = 70,
          z buffer  = sort,
          colormap/cool,
        ]
        ( {(6+(sin(3*(x+3*y))+1.25)*cos(x))*cos(y)},
          {(6+(sin(3*(x+3*y))+1.25)*cos(x))*sin(y)},
          {((sin(3*(x+3*y))+1.25)*sin(x))} );
        \end{axis}
    \end{tikzpicture}
    \end{document}


<!-- The completed figure come here

 A short explanation about the piece of code making the plot twist would be useful
 I apologize not to be able to write it myself
 The end -->

PS : cette réponse est une adaptation de celle citée plus haut ainsi que de
[celle-ci](https://tex.stackexchange.com/questions/192513/rotation-transformation-of-a-parametrized-plot).
